/*
GNU General Public License version 3 notice

Copyright (C) 2013 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "QuakeUtil.h"
#include <QString>
#include <QRegExp>

char QuakeUtil::ourReadableCharsTable[256] = {
  '.', '_' , '_' , '_' , '_' , '.' , '_' , '_' ,
  '_' , '_' , '\n' , '_' , '\n' , '>' , '.' , '.',
  '[', ']', '0', '1', '2', '3', '4', '5', '6',
  '7', '8', '9', '.', '_', '_', '_' };

bool QuakeUtil::ourReadableCharsTableInitialized = false;

QuakeUtil::QuakeUtil()
{
}
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void QuakeUtil::initReadableCharsTable()
{
  int i;

  for(i = 32; i < 127; i++)
    ourReadableCharsTable[i] = ourReadableCharsTable[128 + i] = i;
  ourReadableCharsTable[127] = ourReadableCharsTable[128 + 127] = '_';

  for(i = 0; i < 32; i++)
    ourReadableCharsTable[128 + i] = ourReadableCharsTable[i];
  ourReadableCharsTable[128] = '_';
  ourReadableCharsTable[10 + 128] = '_';
  ourReadableCharsTable[12 + 128] = '_';

  ourReadableCharsTableInitialized = true;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
static QRegExp rxColoredText("&c[A-Fa-f0-9]{3}");

QString QuakeUtil::parseNameFun(const QString &string)
{
  if(!ourReadableCharsTableInitialized)
    initReadableCharsTable();

  QString stripped;

  for(int i = 0; i < string.length(); i++)
    stripped.append(QChar(ourReadableCharsTable[(unsigned char)string.at(i).toAscii()] & 127));

  // Now remove the &cfxxx string for those colored texts
  stripped.remove(rxColoredText);

  return stripped;
}
