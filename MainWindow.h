/*
GNU General Public License version 3 notice

Copyright (C) 2013 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>

namespace Ui {
  class MainWindow;
}


/**
 * @brief
 *
 */
class MainWindow : public QMainWindow
{
  Q_OBJECT
  
public:
  /**
   * @brief
   *
   * @param parent
   */
  explicit MainWindow(QWidget *parent = 0);
  /**
   * @brief
   *
   */
  ~MainWindow();

protected:
  void changeEvent(QEvent *e);

private slots:
  /**
   * @brief
   *
   */
  void on_actionE_xit_triggered();

  /**
   * @brief
   *
   */
  void on_action_Config_triggered();

  void on_action_About_triggered();

  void on_actionAbout_Qt_triggered();

  void on_trayIcon_activated(QSystemTrayIcon::ActivationReason reason);

  void on_consoleReader_newConsoleData(const QString& text);

private:
  Ui::MainWindow *ui; /**< TODO */
  QSystemTrayIcon* trayIcon;
};

#endif // MAINWINDOW_H
