/*
GNU General Public License version 3 notice

Copyright (C) 2013 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef QUAKECONSOLEREADER_H
#define QUAKECONSOLEREADER_H

#include <QTextEdit>

class QFile;
class QLocalSocket;
/**
 * @brief
 *
 */
class QuakeConsoleReader : public QTextEdit
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param parent
   */
  explicit QuakeConsoleReader(QWidget *parent = 0);
  /**
   * @brief
   *
   */
  ~QuakeConsoleReader();

  /**
   * @brief
   *
   */
  void resetCondebug();

signals:
  void newConsoleData(const QString& text);

public slots:
  /**
   * @brief
   *
   */
  void readConsoleData();

private:
  QFile *condebug; /**< condebug.log file */
  // TODO: change to QSocketNotifier for no need of Networking.
  QLocalSocket* socketNotifier; /**< notifies and reads changes on condebug.log file */
};

#endif // QUAKECONSOLEREADER_H
