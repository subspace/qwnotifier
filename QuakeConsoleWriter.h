/*
GNU General Public License version 3 notice

Copyright (C) 2013 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#ifndef QUAKECONSOLEWRITER_H
#define QUAKECONSOLEWRITER_H

#include <QLineEdit>

/**
 * @brief
 *
 */
class QuakeConsoleWriter : public QLineEdit
{
  Q_OBJECT
public:
  /**
   * @brief
   *
   * @param parent
   */
  explicit QuakeConsoleWriter(QWidget *parent = 0);
  
signals:
  
private slots:
  void sendText();
  
};

#endif // QUAKECONSOLEWRITER_H
