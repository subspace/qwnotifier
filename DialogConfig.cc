/*
GNU General Public License version 3 notice

Copyright (C) 2013 Washu. All rights reserved.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see < http://www.gnu.org/licenses/ >.
*/
#include "DialogConfig.h"
#include "ui_DialogConfig.h"
#include <QFileDialog>
#include <QSettings>
#include <QIntValidator>

DialogConfig::DialogConfig(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::DialogConfig)
{
  ui->setupUi(this);
  QSettings s;
  ui->lineEdit->setText(s.value("quake_directory").toString());
  ui->lineXDisplay->setValidator(new QIntValidator(0, 99, ui->lineXDisplay));
  ui->lineXDisplay->setText(s.value("x_display", "0").toString());
}

DialogConfig::~DialogConfig()
{
  delete ui;
}

void DialogConfig::on_toolButton_clicked()
{
  QString quakeDir = QFileDialog::getExistingDirectory(this, tr("Please select Quake folder..."));
  ui->lineEdit->setText(quakeDir);
}

void DialogConfig::accept()
{
  QSettings s;
  s.setValue("quake_directory", ui->lineEdit->text());
  s.setValue("x_display", ui->lineXDisplay->text());
  QDialog::accept();
}
