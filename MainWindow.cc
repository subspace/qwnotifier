//global teste

#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "DialogConfig.h"
#include <QMessageBox>
#include <QMenu>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  trayIcon(new QSystemTrayIcon(this))
{
  trayIcon->setObjectName("trayIcon");
  trayIcon->setIcon(QIcon(":/ezn_icon_enabled.ico"));
  trayIcon->show();

  ui->setupUi(this);
  setWindowTitle("QW Notifier");

  QMenu* menu = new QMenu(this);
  menu->addActions(QList<QAction*>() << ui->actionE_xit);
  trayIcon->setContextMenu(menu);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_consoleReader_newConsoleData(const QString &text)
{
  trayIcon->showMessage(tr("Server Activity"), text);
}

void MainWindow::on_trayIcon_activated(QSystemTrayIcon::ActivationReason reason)
{
  switch(reason) {
  case QSystemTrayIcon::DoubleClick:
//    QTimer::singleShot(0, this, SLOT(showNormal()));
    showNormal();
    break;
  default:
    break;
  }
}

void MainWindow::on_actionE_xit_triggered()
{
  close();
}

void MainWindow::on_action_Config_triggered()
{
  DialogConfig d;
  d.exec();
  ui->consoleReader->resetCondebug();
}

void MainWindow::on_action_About_triggered()
{
  QMessageBox::about(this, tr("About"), tr("QW Notifier by Washu and AL.Kernell"));
}

void MainWindow::on_actionAbout_Qt_triggered()
{
  QMessageBox::aboutQt(this);
}

void MainWindow::changeEvent(QEvent *e)
{
  switch(e->type()) {
  case QEvent::WindowStateChange:
    if(windowState() & Qt::WindowMinimized) {
      hide();
//      QTimer::singleShot(0, this, SLOT(hide()));
    }
    break;
  default:
    break;
  }
  QMainWindow::changeEvent(e);
}
